var Theme = require('./theme');

/**
 *  Array of theme objects
 *
 * @type {Array}
 */
var themes = [];

var ThemeRegistry = function() {

};

ThemeRegistry.list = function () {
  console.log(themes);
};

ThemeRegistry.register = function(theme_name, view) {
  var theme = new Theme(theme_name, view);
  themes.push(theme);
};

ThemeRegistry.load = function(theme_name, callback) {
  themes.forEach(function (theme) {
    if (theme.name === theme_name) {
      callback(theme);
    }
  });
};

module.exports = ThemeRegistry;