var app = require('../index');

var Theme = function(name, view) {
  /**
   * Unique theme machine name
   */
  this.name = name;

  /**
   * Template path, relative to app views directory.
   */
  this.view = view;
};

Theme.prototype.render = function(options, callback) {
  app.render(this.view, options, function(err, html) {
    if (err) {
      console.log(err);
      return callback(err);
    }

    console.log(html);
    callback(err, html);
  });
};

module.exports = Theme;
