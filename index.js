require('register-module')({
  name: 'theme-registry',
  path: __dirname + '/core',
  main: 'theme_registry.js'
});

var express = require('express');
var morgan = require('morgan');
var app = module.exports = express();

var theme_registry = require('theme-registry');

theme_registry.register('users', 'users/users');

app.use('theme_registry', theme_registry);

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

var users = require('./routes/users');

app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));

app.use('/users', users);

app.get('/', function (req, res) {
   res.render('index', {
      title: 'Home'
   })
});

app.listen(3000);
