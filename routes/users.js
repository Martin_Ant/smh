var express = require('express');
var router = express.Router();
var theme_registry = require('theme-registry');

router.get('/', function (req, res) {
  theme_registry.load('users', function (theme) {
    theme.render({
      create_link: req.baseUrl + '/add',
      title: "Users",
      users: [
        {
          uid: 1,
          username: 'User 1'
        },
        {
          uid: 3,
          username: 'User 2',
          links: [
            {
              title: 'Edit',
              path: req.baseUrl + '/edit/3'
            }
          ]
        }
      ]
    }, function(err, html) {
      if (!err) {
        res.send(html);
      }
    });
  });
});

router.get('/add', function (req, res) {

});

module.exports = router;
